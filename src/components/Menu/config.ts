import { MenuEntry } from '@pizzafinance/ui-sdk'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange.longswap.org',
      },
      {
        label: 'Liquidity',
        href: 'https://longswap.org/#/pool',
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Pools',
    icon: 'PoolIcon',
    href: '/pools',
  },
  {
    label: 'Lottery',
    icon: 'TicketIcon',
    href: '/lottery',
  },
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'Overview',
        href: 'https://info.longswap.org',
      },
      {
        label: 'Tokens',
        href: 'https://info.longswap.org/tokens',
      },
      {
        label: 'Pairs',
        href: 'https://info.longswap.org/pairs',
      },
      {
        label: 'Accounts',
        href: 'https://info.longswap.org/accounts',
      },
    ],
  },
  {
  label: 'IFO',
  icon: 'IfoIcon',
  href: '/ifo',
  },
  {
    label: 'More',
    icon: 'MoreIcon',
    items: [
      {
        label: 'Github',
        href: 'https://github.com/longswapbsc',
      },
    ],
  },
]

export default config
